# Informações do usuário

## Visualizando informações

Ao realizar o login no aplicativo, as informações pessoais do usuário serão exibidas na tela inicial, de forma similar a apresentada na imagem a seguir:

<figure class="images mobile-images">
    <img src="../assets/images/operacional-home.png" />
</figure>
